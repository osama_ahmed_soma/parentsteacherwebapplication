import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {Routes} from './app.routing';

// Services
import {AuthService} from './_service/auth/auth.service';
import {IpService} from './_service/ip/ip.service';

import {AppComponent} from './app.component';
import {HeaderComponent} from './header/header.component';
import {HomeComponent} from './home/home.component';
import {ProductComponent} from './product/product.component';
import {LoginComponent} from './login/login.component';
import {ParentDashboardComponent} from './parent-dashboard/parent-dashboard.component';
import {TeacherDashboardComponent} from './teacher-dashboard/teacher-dashboard.component';
import {FAQComponent} from './faq/faq.component';
import {ContactUsComponent} from './contact-us/contact-us.component';
import {NavBarComponent} from './nav-bar/nav-bar.component';
import {MyContainerComponent} from './my-container/my-container.component';
import {FooterComponent} from './footer/footer.component';
import { MessagesComponent } from './messages/messages/messages.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    ProductComponent,
    LoginComponent,
    ParentDashboardComponent,
    TeacherDashboardComponent,
    FAQComponent,
    ContactUsComponent,
    NavBarComponent,
    MyContainerComponent,
    FooterComponent,
    MessagesComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    Routes
  ],
  providers: [
    AuthService,
    IpService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
