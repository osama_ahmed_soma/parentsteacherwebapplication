import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

// Config
import {Config} from '../../config/main.config';

// Interfaces
import {LoginFormUserDataInteface} from '../../_interfaces/LoginFormUserDataInteface';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  apiURL: string = Config.api;

  constructor(private http: HttpClient) {
  }

  login(userData: LoginFormUserDataInteface) {
    const headers = new HttpHeaders({
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    });
    const loginCredentials = {
      grant_type: 'password',
      client_id: Config.authClientCredentials.client_id,
      client_secret: Config.authClientCredentials.client_secret,
      username: userData.p_phoneNumber,
      password: userData.p_password
    };
    return this.http.post(Config.domain + '/oauth/token', loginCredentials, {
      headers
    });
  }

}
