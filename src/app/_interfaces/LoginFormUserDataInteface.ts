export interface LoginFormUserDataInteface {
  p_phoneNumber: string;
  p_password: string;
  rememberMe: boolean;
}
