import {RouterModule} from '@angular/router';

import {HomeComponent} from './home/home.component';
import {HeaderComponent} from './header/header.component';
import {ProductComponent} from './product/product.component';
import {FAQComponent} from './faq/faq.component';
import {ContactUsComponent} from './contact-us/contact-us.component';
import {LoginComponent} from './login/login.component';
import {ParentDashboardComponent} from './parent-dashboard/parent-dashboard.component';
import {TeacherDashboardComponent} from './teacher-dashboard/teacher-dashboard.component';
import {MessagesComponent} from "./messages/messages/messages.component";

export const Routes = RouterModule.forRoot([
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'product',
    component: ProductComponent
  },
  {
    path: 'faq',
    component: FAQComponent
  },
  {
    path: 'contact_us',
    component: ContactUsComponent
  },
  {
    path: 'header',
    component: HeaderComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'parent-dashboard',
    component: ParentDashboardComponent
  },
  {
    path: 'teacher-dashboard',
    component: TeacherDashboardComponent
  },
  {
    path: 'messages',
    component: MessagesComponent
  }
]);
