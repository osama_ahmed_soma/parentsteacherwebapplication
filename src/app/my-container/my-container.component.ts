import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-my-container',
  templateUrl: './my-container.component.html',
  styleUrls: ['./my-container.component.css']
})
export class MyContainerComponent implements OnInit {

  @Input() isHomePage: boolean = false;
  @Input() extraClasses: string = '';

  constructor() { }

  ngOnInit() {
  }

}
