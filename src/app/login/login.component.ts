import {Component, OnInit} from '@angular/core';

// Interfaces
import {LoginFormUserDataInteface} from '../_interfaces/LoginFormUserDataInteface';

// Services
import {AuthService} from '../_service/auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  userData: LoginFormUserDataInteface = {
    p_phoneNumber: '',
    p_password: '',
    rememberMe: false
  };

  constructor(private _authService: AuthService) {
  }

  ngOnInit() {
  }

  private validateUserData(): boolean {
    let isError: boolean = false;
    if (this.userData.p_phoneNumber.trim() === '') {
      alert('Phone Number is required.');
      isError = true;
    }
    if (this.userData.p_password.trim() === '') {
      alert('Password is required.');
      isError = true;
    }
    return isError;

  }

  loginFormSubmit() {
    if (this.validateUserData()) {
      return;
    }
    // show loading or something

    // service call
    // rest api hit
    this._authService.login(this.userData).subscribe((res) => {
      console.log(res);
    });
  }

}
